TERM = dumb
SHELL = bash
MAKE = make
LIBTICS_DIR = ../../../..
GN = $(LIBTICS_DIR)/utils/bin/gen_navdoc
AAPL_DIR = ../aapl/modules/ROOT/pages

local-site: gen_navigation
	antora -v
	antora --stacktrace generate \
		--log-level  all\
		--log-failure-level error \
		--asciidoc-sourcemap \
		--clean --fetch \
		--to-dir build/site \
		ant-libtics-local.yml

gen_navigation:
	cd $(AAPL_DIR) && make gen_navigation && cd -

# There is an assumption that local-site will have been generated for testing,
# that gen_navigation will have been invoked to generate a nav.adoc file
# that is updated in the repo with git prior to remote-site being invoked on
# Netlify.
remote-site:
	npm install && npm update
	antora -v
	antora --stacktrace generate \
		--log-level all \
		--log-failure-level error \
		--asciidoc-sourcemap \
		--clean --fetch \
		--to-dir build/site \
		ant-libtics-remote.yml

